<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Feature\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Providers\ServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class ItemCreateTest extends TestCase
{
    use RefreshDatabase;

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    /**
     * @dataProvider invalidRequestDataProvider
     */
    public function testInvalidRequest(array $params, array $invalidFields)
    {
        $response = $this->post('/api/items', $params);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors($invalidFields);
    }

    public function invalidRequestDataProvider()
    {
        return [
            [
                ['name' => '', 'amount' => 0],
                ['name']
            ],
            [
                ['name' => 'a', 'amount' => 1],
                ['name']
            ],
            [
                ['name' => 'foo', 'amount' => ''],
                ['amount']
            ],
            [
                ['name' => 'foo', 'amount' => -1],
                ['amount']
            ],
            [
                ['name' => '', 'amount' => ''],
                ['name', 'amount']
            ],
            [
                ['name' => 'a', 'amount' => -1],
                ['name', 'amount']
            ]
        ];
    }

    /**
     * @dataProvider validRequestDataProvider
     */
    public function testValidRequest(array $params, array $expectedJson)
    {
        $response = $this->post('/api/items', $params);

        $response->assertStatus(200);
        $response->assertJson($expectedJson);
    }

    public function validRequestDataProvider()
    {
        return [
            [
                ['name' => 'foo', 'amount' => 0],
                ['id' => 1, 'name' => 'foo', 'amount' => 0]
            ],
            [
                ['name' => 'bar', 'amount' => 1],
                ['id' => 1, 'name' => 'bar', 'amount' => 1]
            ],
            [
                ['name' => 'baz', 'amount' => 5],
                ['id' => 1, 'name' => 'baz', 'amount' => 5]
            ]
        ];
    }
}
