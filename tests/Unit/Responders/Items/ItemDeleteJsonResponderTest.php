<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Responders\Items;

use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemDeleteJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\App;
use Orchestra\Testbench\TestCase;

class ItemDeleteJsonResponderTest extends TestCase
{
    /**
     * @dataProvider responseDataProvider
     */
    public function testResponse(array $errors, bool $shouldReturnOk200)
    {
        $itemDeleteJsonResponder = new ItemDeleteJsonResponder(App::make(ResponseFactory::class));

        $response = $itemDeleteJsonResponder->respond($errors);

        $this->assertInstanceOf(JsonResponse::class, $response);

        if ($shouldReturnOk200) {
            $this->assertSame(200, $response->getStatusCode());
            $this->assertSame(json_encode([]), $response->getContent());
        } else {
            $this->assertSame(422, $response->getStatusCode());
            $this->assertSame(json_encode(['errors' => $errors]), $response->getContent());
        }
    }

    public function responseDataProvider(): array
    {
        $errors = [
            'id' => ['Error']
        ];

        return [
            [$errors, false],
            [[], true]
        ];
    }
}
