<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Actions\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Actions\Api\Items\ItemUpdateAction;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemUpdateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateUpdateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemUpdateJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;

class ItemUpdateActionTest extends TestCase
{
    public function testValidRequestWithSuccess()
    {
        $id = 1;

        $requestParams = [
            'name' => 'foo',
            'amount' => 1
        ];

        $validateServiceErrors = [];

        $item = new Item(
            new ItemId($id),
            new ItemName($requestParams['name']),
            new ItemAmount($requestParams['amount'])
        );

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $requestMock = $this->createMock(Request::class);
        $requestMock->expects($this->once())
            ->method('all')
            ->willReturn($requestParams);

        $requestParams['id'] = $id;

        $validateServiceMock = $this->createMock(ItemValidateUpdateService::class);
        $validateServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($requestParams))
            ->willReturn($validateServiceErrors);

        $createServiceMock = $this->createMock(ItemUpdateService::class);
        $createServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($item))
            ->willReturn(true);

        $responderMock = $this->createMock(ItemUpdateJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($validateServiceErrors), $this->equalTo($item))
            ->willReturn($jsonResponseMock);

        $itemUpdateAction = new ItemUpdateAction($requestMock, $validateServiceMock, $createServiceMock, $responderMock);
        $this->assertSame($jsonResponseMock, $itemUpdateAction($id));
    }

    public function testValidRequestWithFail()
    {
        $id = 1;

        $requestParams = [
            'name' => 'foo',
            'amount' => 1
        ];

        $validateServiceErrors = [];

        $item = new Item(
            new ItemId($id),
            new ItemName($requestParams['name']),
            new ItemAmount($requestParams['amount'])
        );

        $updateFailMessage = 'Error!';

        $updateFailErrors = [
            'error' => $updateFailMessage
        ];

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $requestMock = $this->createMock(Request::class);
        $requestMock->expects($this->once())
            ->method('all')
            ->willReturn($requestParams);

        $requestParams['id'] = $id;

        $validateServiceMock = $this->createMock(ItemValidateUpdateService::class);
        $validateServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($requestParams))
            ->willReturn($validateServiceErrors);

        $createServiceMock = $this->createMock(ItemUpdateService::class);
        $createServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($item))
            ->willReturn(false);

        $responderMock = $this->createMock(ItemUpdateJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($updateFailErrors), $this->equalTo($item))
            ->willReturn($jsonResponseMock);

        $itemUpdateAction = new ItemUpdateAction($requestMock, $validateServiceMock, $createServiceMock, $responderMock, $updateFailMessage);
        $this->assertSame($jsonResponseMock, $itemUpdateAction($id));
    }

    public function testInvalidRequest()
    {
        $id = '';

        $requestParams = [
            'name' => '',
            'amount' => -1
        ];

        $validateServiceErrors = [
            'errors' => [
                'id' => ['error'],
                'name' => ['error'],
                'amount' => ['error']
            ]
        ];

        $item = null;

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $requestMock = $this->createMock(Request::class);
        $requestMock->expects($this->once())
            ->method('all')
            ->willReturn($requestParams);

        $requestParams['id'] = $id;

        $validateServiceMock = $this->createMock(ItemValidateUpdateService::class);
        $validateServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($requestParams))
            ->willReturn($validateServiceErrors);

        $createServiceMock = $this->createMock(ItemUpdateService::class);
        $createServiceMock->expects($this->never())
            ->method('run');

        $responderMock = $this->createMock(ItemUpdateJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($validateServiceErrors), $this->equalTo($item))
            ->willReturn($jsonResponseMock);

        $itemUpdateAction = new ItemUpdateAction($requestMock, $validateServiceMock, $createServiceMock, $responderMock);
        $this->assertSame($jsonResponseMock, $itemUpdateAction($id));
    }
}
