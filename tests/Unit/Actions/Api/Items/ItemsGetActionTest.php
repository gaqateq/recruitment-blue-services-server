<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Actions\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Actions\Api\Items\ItemsGetAction;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemsGetAvailableService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemsGetServiceFactory;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemsGetJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class ItemsGetActionTest extends TestCase
{
    public function testRequestWithTypeParameter()
    {
        $requestParams = [
            'type' => 'available'
        ];

        $collection = new Collection([[1], [2]]);

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $requestMock = $this->createMock(Request::class);
        $requestMock->expects($this->once())
            ->method('get')
            ->with($this->equalTo('type'))
            ->willReturn($requestParams['type']);

        $repositoryMock = $this->createMock(ItemRepository::class);

        $itemsGetAvailableServiceMock = $this->createMock(ItemsGetAvailableService::class);
        $itemsGetAvailableServiceMock->expects($this->once())
            ->method('run')
            ->willReturn($collection);

        $serviceFactoryMock = $this->createMock(ItemsGetServiceFactory::class);
        $serviceFactoryMock->expects($this->once())
            ->method('make')
            ->with($this->equalTo($requestParams['type']), $this->equalTo($repositoryMock))
            ->willReturn($itemsGetAvailableServiceMock);

        $responderMock = $this->createMock(ItemsGetJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($collection))
            ->willReturn($jsonResponseMock);

        $itemsGetAction = new ItemsGetAction($requestMock, $repositoryMock, $serviceFactoryMock, $responderMock);
        $this->assertSame($jsonResponseMock, $itemsGetAction());
    }

    public function testRequestWithoutParameters()
    {
        $collection = new Collection([[1], [2]]);

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $requestMock = $this->createMock(Request::class);
        $requestMock->expects($this->once())
            ->method('get')
            ->with($this->equalTo('type'))
            ->willReturn(null);

        $repositoryMock = $this->createMock(ItemRepository::class);

        $itemsGetAvailableServiceMock = $this->createMock(ItemsGetAvailableService::class);
        $itemsGetAvailableServiceMock->expects($this->once())
            ->method('run')
            ->willReturn($collection);

        $serviceFactoryMock = $this->createMock(ItemsGetServiceFactory::class);
        $serviceFactoryMock->expects($this->once())
            ->method('make')
            ->with($this->equalTo(null), $this->equalTo($repositoryMock))
            ->willReturn($itemsGetAvailableServiceMock);

        $responderMock = $this->createMock(ItemsGetJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($collection))
            ->willReturn($jsonResponseMock);

        $itemsGetAction = new ItemsGetAction($requestMock, $repositoryMock, $serviceFactoryMock, $responderMock);
        $this->assertSame($jsonResponseMock, $itemsGetAction());
    }
}
