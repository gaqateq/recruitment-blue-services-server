<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Actions\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Actions\Api\Items\ItemDeleteAction;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemDeleteService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateDeleteService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemDeleteJsonResponder;
use Illuminate\Http\JsonResponse;
use PHPUnit\Framework\TestCase;

class ItemDeleteActionTest extends TestCase
{
    public function testValidRequestWithSuccess()
    {
        $id = 1;

        $requestParams = [
            'id' => $id
        ];

        $validateServiceErrors = [];

        $itemId = new ItemId($id);

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $validateServiceMock = $this->createMock(ItemValidateDeleteService::class);
        $validateServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($requestParams))
            ->willReturn($validateServiceErrors);

        $createServiceMock = $this->createMock(ItemDeleteService::class);
        $createServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($itemId))
            ->willReturn(true);

        $responderMock = $this->createMock(ItemDeleteJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($validateServiceErrors))
            ->willReturn($jsonResponseMock);

        $itemDeleteAction = new ItemDeleteAction($validateServiceMock, $createServiceMock, $responderMock);
        $this->assertSame($jsonResponseMock, $itemDeleteAction($id));
    }

    public function testValidRequestWithFail()
    {
        $id = 1;

        $requestParams = [
            'id' => $id
        ];

        $validateServiceErrors = [];

        $itemId = new ItemId($id);

        $deleteFailMessage = 'Error!';

        $deleteFailErrors = [
            'error' => $deleteFailMessage
        ];

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $validateServiceMock = $this->createMock(ItemValidateDeleteService::class);
        $validateServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($requestParams))
            ->willReturn($validateServiceErrors);

        $createServiceMock = $this->createMock(ItemDeleteService::class);
        $createServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($itemId))
            ->willReturn(false);

        $responderMock = $this->createMock(ItemDeleteJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($deleteFailErrors))
            ->willReturn($jsonResponseMock);

        $itemDeleteAction = new ItemDeleteAction($validateServiceMock, $createServiceMock, $responderMock, $deleteFailMessage);
        $this->assertSame($jsonResponseMock, $itemDeleteAction($id));
    }

    public function testInvalidRequest()
    {
        $id = '';

        $requestParams = [
            'id' => $id
        ];

        $validateServiceErrors = [
            'errors' => [
                'id' => ['error']
            ]
        ];

        $jsonResponseMock = $this->createMock(JsonResponse::class);

        $validateServiceMock = $this->createMock(ItemValidateDeleteService::class);
        $validateServiceMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($requestParams))
            ->willReturn($validateServiceErrors);

        $createServiceMock = $this->createMock(ItemDeleteService::class);
        $createServiceMock->expects($this->never())
            ->method('run');

        $responderMock = $this->createMock(ItemDeleteJsonResponder::class);
        $responderMock->expects($this->once())
            ->method('respond')
            ->with($this->equalTo($validateServiceErrors))
            ->willReturn($jsonResponseMock);

        $itemDeleteAction = new ItemDeleteAction($validateServiceMock, $createServiceMock, $responderMock);
        $this->assertSame($jsonResponseMock, $itemDeleteAction($id));
    }
}
