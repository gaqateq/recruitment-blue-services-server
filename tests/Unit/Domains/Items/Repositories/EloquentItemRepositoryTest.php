<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Repositories;

use App\Models\Item as ItemModel;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\EloquentItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Providers\ServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Orchestra\Testbench\TestCase;

class EloquentItemRepositoryTest extends TestCase
{
    use RefreshDatabase;

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    public function testFind()
    {
        $expectedItem = new Item(
            new ItemId(1),
            new ItemName('foo'),
            new ItemAmount(1)
        );

        ItemModel::create([
            'name' => $expectedItem->getName()->getValue(),
            'amount' => $expectedItem->getAmount()->getValue()
        ]);

        $eloquentItemRepository = new EloquentItemRepository(new ItemModel());
        $foundItem = $eloquentItemRepository->find($expectedItem->getId());

        $this->assertEquals($expectedItem, $foundItem);
    }

    public function testCreate()
    {
        $expectedItem = new Item(
            new ItemId(1),
            new ItemName('foo'),
            new ItemAmount(1)
        );

        $eloquentItemRepository = new EloquentItemRepository(new ItemModel());
        $createdItem = $eloquentItemRepository->create($expectedItem->getName(), $expectedItem->getAmount());

        $this->assertDatabaseHas('items', [
            'name' => $expectedItem->getName()->getValue(),
            'amount' => $expectedItem->getAmount()->getValue()
        ]);

        $this->assertEquals($expectedItem, $createdItem);
    }

    public function testCreateWithId()
    {
        $expectedItem = new Item(
            new ItemId(99),
            new ItemName('foo'),
            new ItemAmount(10)
        );

        $eloquentItemRepository = new EloquentItemRepository(new ItemModel());
        $createdItem = $eloquentItemRepository->createWithId($expectedItem);

        $this->assertDatabaseHas('items', [
            'id' => $expectedItem->getId()->getValue(),
            'name' => $expectedItem->getName()->getValue(),
            'amount' => $expectedItem->getAmount()->getValue()
        ]);
    }

    public function testUpdate()
    {
        $expectedItem = new Item(
            new ItemId(1),
            new ItemName('foo'),
            new ItemAmount(1)
        );

        ItemModel::create([
            'name' => $expectedItem->getName()->getValue(),
            'amount' => $expectedItem->getAmount()->getValue()
        ]);

        $expectedItem->setName(new ItemName('bar'))
            ->setAmount(new ItemAmount(5));

        $eloquentItemRepository = new EloquentItemRepository(new ItemModel());
        $this->assertTrue($eloquentItemRepository->update($expectedItem));

        $this->assertDatabaseHas('items', [
            'name' => $expectedItem->getName()->getValue(),
            'amount' => $expectedItem->getAmount()->getValue()
        ]);

        $nonExistingItem = new Item(
            new ItemId(2),
            new ItemName('baz'),
            new ItemAmount(10)
        );

        $this->assertFalse($eloquentItemRepository->update($nonExistingItem));
    }

    public function testRemove()
    {
        $itemId = new ItemId(1);

        ItemModel::create([
            'name' => 'foo',
            'amount' => 1
        ]);

        $eloquentItemRepository = new EloquentItemRepository(new ItemModel());
        $this->assertTrue($eloquentItemRepository->remove($itemId));

        $this->assertDatabaseMissing('items', [
            'id' => $itemId->getValue()
        ]);

        $this->assertFalse($eloquentItemRepository->remove($itemId));
    }

    public function testAvailable()
    {
        $expectedCollection = new Collection([
            new Item(
                new ItemId(1),
                new ItemName('foo'),
                new ItemAmount(1)
            ),
            new Item(
                new ItemId(2),
                new ItemName('bar'),
                new ItemAmount(2)
            ),
            new Item(
                new ItemId(3),
                new ItemName('baz'),
                new ItemAmount(3)
            ),
        ]);

        $notExpectedCollection = new Collection([
            new Item(
                new ItemId(4),
                new ItemName('foo 1'),
                new ItemAmount(0)
            ),
            new Item(
                new ItemId(5),
                new ItemName('bar 1'),
                new ItemAmount(0)
            ),
            new Item(
                new ItemId(6),
                new ItemName('baz 1'),
                new ItemAmount(0)
            ),
        ]);

        foreach ($expectedCollection as $item) {
            ItemModel::create([
                'name' => $item->getName()->getValue(),
                'amount' => $item->getAmount()->getValue()
            ]);
        }

        foreach ($notExpectedCollection as $item) {
            ItemModel::create([
                'name' => $item->getName()->getValue(),
                'amount' => $item->getAmount()->getValue()
            ]);
        }

        $eloquentItemRepository = new EloquentItemRepository(new ItemModel());
        $this->assertEquals($expectedCollection->toArray(), $eloquentItemRepository->available()->toArray());
    }

    public function testUnavailable()
    {
        $expectedCollection = new Collection([
            new Item(
                new ItemId(1),
                new ItemName('foo'),
                new ItemAmount(0)
            ),
            new Item(
                new ItemId(2),
                new ItemName('bar'),
                new ItemAmount(0)
            ),
            new Item(
                new ItemId(3),
                new ItemName('baz'),
                new ItemAmount(0)
            ),
        ]);

        $notExpectedCollection = new Collection([
            new Item(
                new ItemId(4),
                new ItemName('foo 1'),
                new ItemAmount(1)
            ),
            new Item(
                new ItemId(5),
                new ItemName('bar 1'),
                new ItemAmount(2)
            ),
            new Item(
                new ItemId(6),
                new ItemName('baz 1'),
                new ItemAmount(3)
            ),
        ]);

        foreach ($expectedCollection as $item) {
            ItemModel::create([
                'name' => $item->getName()->getValue(),
                'amount' => $item->getAmount()->getValue()
            ]);
        }

        foreach ($notExpectedCollection as $item) {
            ItemModel::create([
                'name' => $item->getName()->getValue(),
                'amount' => $item->getAmount()->getValue()
            ]);
        }

        $eloquentItemRepository = new EloquentItemRepository(new ItemModel());
        $this->assertEquals($expectedCollection->toArray(), $eloquentItemRepository->unavailable()->toArray());
    }

    public function testMoreThanFive()
    {
        $expectedCollection = new Collection([
            new Item(
                new ItemId(1),
                new ItemName('foo'),
                new ItemAmount(6)
            ),
            new Item(
                new ItemId(2),
                new ItemName('bar'),
                new ItemAmount(7)
            ),
            new Item(
                new ItemId(3),
                new ItemName('baz'),
                new ItemAmount(8)
            ),
        ]);

        $notExpectedCollection = new Collection([
            new Item(
                new ItemId(4),
                new ItemName('foo 1'),
                new ItemAmount(0)
            ),
            new Item(
                new ItemId(5),
                new ItemName('bar 1'),
                new ItemAmount(2)
            ),
            new Item(
                new ItemId(6),
                new ItemName('baz 1'),
                new ItemAmount(4)
            ),
        ]);

        foreach ($expectedCollection as $item) {
            ItemModel::create([
                'name' => $item->getName()->getValue(),
                'amount' => $item->getAmount()->getValue()
            ]);
        }

        foreach ($notExpectedCollection as $item) {
            ItemModel::create([
                'name' => $item->getName()->getValue(),
                'amount' => $item->getAmount()->getValue()
            ]);
        }

        $eloquentItemRepository = new EloquentItemRepository(new ItemModel());
        $this->assertEquals($expectedCollection->toArray(), $eloquentItemRepository->moreThanFive()->toArray());
    }
}
