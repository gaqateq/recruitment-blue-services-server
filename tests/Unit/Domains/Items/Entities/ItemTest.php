<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Entities;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use PHPUnit\Framework\TestCase;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;

class ItemTest extends TestCase
{
    public function testGetters()
    {
        $itemId = new ItemId(1);

        $itemName = new ItemName('foo');

        $itemAmount = new ItemAmount(1);

        $item = new Item($itemId, $itemName, $itemAmount);

        $this->assertSame($itemId, $item->getId());
        $this->assertSame($itemName, $item->getName());
        $this->assertSame($itemAmount, $item->getAmount());
    }

    public function testSetters()
    {
        $item = new Item(
            new ItemId(1),
            new ItemName('foo'),
            new ItemAmount(1)
        );

        $expectedItemName = new ItemName('bar');
        $expectedItemAmount = new ItemAmount(5);

        $item->setName($expectedItemName)
            ->setAmount($expectedItemAmount);

        $this->assertSame($expectedItemName, $item->getName());
        $this->assertSame($expectedItemAmount, $item->getAmount());
    }

    public function testToArray()
    {
        $itemArray = [
            'id' => 1,
            'name' => 'foo',
            'amount' => 1
        ];

        $itemId = new ItemId($itemArray['id']);

        $itemName = new ItemName($itemArray['name']);

        $itemAmount = new ItemAmount($itemArray['amount']);

        $item = new Item($itemId, $itemName, $itemAmount);

        $this->assertSame($itemArray, $item->toArray());
    }
}
