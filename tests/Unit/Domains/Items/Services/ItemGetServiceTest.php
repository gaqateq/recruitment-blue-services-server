<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemGetService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use PHPUnit\Framework\TestCase;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;

class ItemGetServiceTest extends TestCase
{
    public function testIfRepositoryValueIsReturned()
    {
        $itemId = new ItemId(1);

        $item = new Item(
            $itemId,
            new ItemName('foo'),
            new ItemAmount(10)
        );

        $repositoryMock = $this->createMock(ItemRepository::class);
        $repositoryMock->method('find')
            ->willReturn($item, null);

        $itemGetService = new ItemGetService($repositoryMock);

        $this->assertSame($item, $itemGetService->run($itemId));
        $this->assertSame(null, $itemGetService->run($itemId));
    }

    public function testIfItemIdIsPassedToRepository()
    {
        $itemId = new ItemId(1);

        $repositoryMock = $this->createMock(ItemRepository::class);
        $repositoryMock->expects($this->once())
            ->method('find')
            ->with($this->equalTo($itemId));

        $itemGetService = new ItemGetService($repositoryMock);

        $itemGetService->run($itemId);
    }
}
