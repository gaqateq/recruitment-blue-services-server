<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateDeleteService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Providers\ServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Factory;
use Orchestra\Testbench\TestCase;

class ItemValidateDeleteServiceTest extends TestCase
{
    use RefreshDatabase;

    private const ID_MIN_VALUE = 1;
    private const ID_MAX_VALUE = 2147483647;

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    public function testRequiredParams()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateDeleteService = new ItemValidateDeleteService(App::make(Factory::class), ['required' => $expectedErrorMessage]);
        $errors = $itemValidateDeleteService->run([]);

        $this->assertArrayHasKey('id', $errors);
        $this->assertSame($expectedErrorMessage, $errors['id'][0]);
    }

    public function testValidateIdOnly()
    {
        $itemId = new ItemId(1);

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId(
            new Item(
                $itemId,
                new ItemName('foo'),
                new ItemAmount(1)
            )
        );

        $itemValidateDeleteService = new ItemValidateDeleteService(App::make(Factory::class));
        $this->assertEmpty($itemValidateDeleteService->run(['id' => $itemId->getValue()]));
    }

    /**
     * @dataProvider idIsIntegerValidationProvider
     */
    public function testIdIsIntegerValidation($id, bool $expectErrors)
    {
        $expectedErrorMessage = 'test fail!';

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId(
            new Item(
                new ItemId(1),
                new ItemName('foo'),
                new ItemAmount(1)
            )
        );

        $itemValidateDeleteService = new ItemValidateDeleteService(App::make(Factory::class), ['integer' => $expectedErrorMessage]);
        $errors = $itemValidateDeleteService->run([
            'id' => $id
        ]);

        if ($expectErrors) {
            $this->assertArrayHasKey('id', $errors);
            $this->assertSame($expectedErrorMessage, $errors['id'][0]);
        } else {
            $this->assertArrayNotHasKey('id', $errors);
        }
    }

    public function idIsIntegerValidationProvider(): array
    {
        return [
            ['foo', true],
            ['1foo', true],
            ['foo1', true],
            [1.1, true],
            [false, true],
            [1, false]
        ];
    }

    public function testIdMinimumMaximumValueValidation()
    {
        $minValue = self::ID_MIN_VALUE;
        $maxValue = self::ID_MAX_VALUE;

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId(
            new Item(
                new ItemId($minValue),
                new ItemName('foo'),
                new ItemAmount(0)
            )
        );
        $itemRepository->createWithId(
            new Item(
                new ItemId($maxValue),
                new ItemName('bar'),
                new ItemAmount(1)
            )
        );

        $itemValidateDeleteService = new ItemValidateDeleteService(App::make(Factory::class));

        $this->assertArrayNotHasKey('id', $itemValidateDeleteService->run(['id' => $minValue]));
        $this->assertArrayNotHasKey('id', $itemValidateDeleteService->run(['id' => $maxValue]));
    }

    public function testIdValueUnderMinimumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateDeleteService = new ItemValidateDeleteService(App::make(Factory::class), ['min' => $expectedErrorMessage]);

        $errors = $itemValidateDeleteService->run(['id' => self::ID_MIN_VALUE - 1]);

        $this->assertArrayHasKey('id', $errors);
        $this->assertSame($expectedErrorMessage, $errors['id'][0]);
    }

    public function testIdValueAboveMaximumValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $itemValidateDeleteService = new ItemValidateDeleteService(App::make(Factory::class), ['max' => $expectedErrorMessage]);

        $errors = $itemValidateDeleteService->run(['id' => self::ID_MAX_VALUE + 1]);

        $this->assertArrayHasKey('id', $errors);
        $this->assertSame($expectedErrorMessage, $errors['id'][0]);
    }

    public function testExistenceValidation()
    {
        $expectedErrorMessage = 'test fail!';

        $itemRepository = App::make(ItemRepository::class);
        $itemRepository->createWithId(
            new Item(
                new ItemId(1),
                new ItemName('foo'),
                new ItemAmount(1)
            )
        );

        $itemValidateDeleteService = new ItemValidateDeleteService(App::make(Factory::class), ['exists' => $expectedErrorMessage]);

        $this->assertArrayNotHasKey('id', $itemValidateDeleteService->run(['id' => 1]));

        $errors = $itemValidateDeleteService->run(['id' => 2]);

        $this->assertArrayHasKey('id', $errors);
        $this->assertSame($expectedErrorMessage, $errors['id'][0]);
    }
}
