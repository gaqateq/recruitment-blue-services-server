<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemsGetUnavailableService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;

class ItemsGetUnavailableServiceTest extends TestCase
{
    /**
     * @dataProvider IfRepositoryValueIsReturnedDataProvider
     */
    public function testIfRepositoryValueIsReturned(Collection $collection)
    {
        $repositoryMock = $this->createMock(ItemRepository::class);
        $repositoryMock->method('unavailable')
            ->willReturn($collection);

        $itemGetService = new ItemsGetUnavailableService($repositoryMock);

        $this->assertSame($collection, $itemGetService->run());
    }

    public function IfRepositoryValueIsReturnedDataProvider(): array
    {
        $item = new Item(
            new ItemId(1),
            new ItemName('foo'),
            new ItemAmount(5)
        );

        $item2 = new Item(
            new ItemId(2),
            new ItemName('bar'),
            new ItemAmount(10)
        );

        return [
            [new Collection([$item, $item2])],
            [new Collection([$item2, $item])]
        ];
    }
}
