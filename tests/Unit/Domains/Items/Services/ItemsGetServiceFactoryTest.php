<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemsGetAvailableService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemsGetMoreThanFiveService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemsGetServiceFactory;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemsGetUnavailableService;
use PHPUnit\Framework\TestCase;

class ItemsGetServiceFactoryTest extends TestCase
{
    public function testCreateItemsGetAvailableService()
    {
        $repositoryMock = $this->createMock(ItemRepository::class);

        $itemsGetServiceFactory = new ItemsGetServiceFactory();
        $this->assertEquals(new ItemsGetAvailableService($repositoryMock), $itemsGetServiceFactory->make('available', $repositoryMock));
    }

    public function testCreateItemsGetUnavailableService()
    {
        $repositoryMock = $this->createMock(ItemRepository::class);

        $itemsGetServiceFactory = new ItemsGetServiceFactory();
        $this->assertEquals(new ItemsGetUnavailableService($repositoryMock), $itemsGetServiceFactory->make('unavailable', $repositoryMock));
    }

    public function testCreateItemsGetMoreThanFiveService()
    {
        $repositoryMock = $this->createMock(ItemRepository::class);

        $itemsGetServiceFactory = new ItemsGetServiceFactory();
        $this->assertEquals(new ItemsGetMoreThanFiveService($repositoryMock), $itemsGetServiceFactory->make('more_than_five', $repositoryMock));
    }

    public function testCreateItemsGetAvailableServiceAsDefault()
    {
        $repositoryMock = $this->createMock(ItemRepository::class);

        $itemsGetServiceFactory = new ItemsGetServiceFactory();
        $this->assertEquals(new ItemsGetAvailableService($repositoryMock), $itemsGetServiceFactory->make('foo', $repositoryMock));
        $this->assertEquals(new ItemsGetAvailableService($repositoryMock), $itemsGetServiceFactory->make(null, $repositoryMock));
    }
}
