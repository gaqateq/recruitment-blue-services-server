<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Tests\Unit\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemUpdateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use PHPUnit\Framework\TestCase;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;

class ItemUpdateServiceTest extends TestCase
{
    public function testIfRepositoryValueIsReturned()
    {
        $item = new Item(
            new ItemId(1),
            new ItemName('foo'),
            new ItemAmount(10)
        );

        $repositoryMock = $this->createMock(ItemRepository::class);
        $repositoryMock->method('update')
            ->willReturn(true, false);

        $itemUpdateService = new ItemUpdateService($repositoryMock);

        $this->assertSame(true, $itemUpdateService->run($item));
        $this->assertSame(false, $itemUpdateService->run($item));
    }

    public function testIfItemIsPassedToRepository()
    {
        $item = new Item(
            new ItemId(1),
            new ItemName('foo'),
            new ItemAmount(10)
        );

        $repositoryMock = $this->createMock(ItemRepository::class);
        $repositoryMock->expects($this->once())
            ->method('update')
            ->with($this->equalTo($item));

        $itemUpdateService = new ItemUpdateService($repositoryMock);

        $itemUpdateService->run($item);
    }
}
