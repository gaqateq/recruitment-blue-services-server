<?php

return [

    /*
     |--------------------------------------------------------------------------
     | Recruitment Blue Services - Server Settings
     |--------------------------------------------------------------------------
     |
     | This is the class that will be used as \Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository implementation
     |
     */

    'item_repository_implementation' => \Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\EloquentItemRepository::class

];
