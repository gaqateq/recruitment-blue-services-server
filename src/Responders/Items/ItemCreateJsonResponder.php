<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Responders\Items;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;

class ItemCreateJsonResponder
{
    /**
     * @var ResponseFactory
     */
    protected $responseFactory;

    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public function respond(array $errors, ?Item $item): JsonResponse
    {
        if (!empty($errors)) {
            return $this->responseFactory->json(['errors' => $errors], 422);
        }

        $content = null === $item ? null : $item->toArray();

        return $this->responseFactory->json($content, 200);
    }
}
