<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Responders\Items;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Collection;

class ItemsGetJsonResponder
{
    /**
     * @var ResponseFactory
     */
    protected $responseFactory;

    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public function respond(Collection $items): JsonResponse
    {
        return $this->responseFactory->json($items->toArray(), 200);
    }
}
