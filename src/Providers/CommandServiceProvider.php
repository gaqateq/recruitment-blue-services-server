<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Providers;

use App\Console\Commands\SeedItems;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class CommandServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        $this->commands(SeedItems::class);
    }
}
