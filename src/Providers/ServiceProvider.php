<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Providers;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class ServiceProvider extends IlluminateServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/recruitmentblueservicesserver.php' => config_path('recruitmentblueservicesserver.php'),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../../config/recruitmentblueservicesserver.php', 'recruitmentblueservicesserver'
        );

        $this->app->bind(
            ItemRepository::class,
            config('recruitmentblueservicesserver.item_repository_implementation')
        );

        $this->app->register(DatabaseServiceProvider::class);
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(CommandServiceProvider::class);
    }
}
