<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;

class ItemUpdateService
{
    /**
     * @var ItemRepository
     */
    protected $repository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->repository = $itemRepository;
    }

    public function run(Item $item): bool
    {
        return $this->repository->update($item);
    }
}
