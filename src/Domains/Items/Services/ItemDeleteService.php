<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;

class ItemDeleteService
{
    /**
     * @var ItemRepository
     */
    protected $repository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->repository = $itemRepository;
    }

    public function run(ItemId $itemId): bool
    {
        return $this->repository->remove($itemId);
    }
}
