<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;

class ItemCreateService
{
    /**
     * @var ItemRepository
     */
    protected $repository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->repository = $itemRepository;
    }

    public function run(ItemName $itemName, ItemAmount $itemAmount): Item
    {
        return $this->repository->create($itemName, $itemAmount);
    }
}
