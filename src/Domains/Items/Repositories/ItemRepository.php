<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Illuminate\Support\Collection;

interface ItemRepository
{
    public function find(ItemId $itemId): ?Item;

    public function create(ItemName $itemName, ItemAmount $itemAmount): Item;

    public function createWithId(Item $item): void;

    public function update(Item $item): bool;

    public function remove(ItemId $itemId): bool;

    public function available(): Collection;

    public function unavailable(): Collection;

    public function moreThanFive(): Collection;
}
