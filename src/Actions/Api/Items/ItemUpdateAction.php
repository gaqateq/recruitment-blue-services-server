<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Actions\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Entities\Item;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemUpdateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateUpdateService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemAmount;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemName;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemUpdateJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ItemUpdateAction
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ItemValidateUpdateService
     */
    protected $validateService;

    /**
     * @var ItemUpdateService
     */
    protected $updateService;

    /**
     * @var ItemUpdateJsonResponder
     */
    protected $responder;

    /**
     * @var string|null
     */
    protected $updateFailMessage = 'Unexpected error. Record not updated.';

    public function __construct(
        Request $request,
        ItemValidateUpdateService $validateService,
        ItemUpdateService $updateService,
        ItemUpdateJsonResponder $responder,
        ?string $updateFailMessage = null
    ) {
        $this->request = $request;
        $this->validateService = $validateService;
        $this->updateService = $updateService;
        $this->responder = $responder;
        if ($updateFailMessage !== null) {
            $this->updateFailMessage = $updateFailMessage;
        }
    }

    public function __invoke($id): JsonResponse
    {
        $item = null;

        $params = $this->request->all();
        $params['id'] = $id;

        $errors = $this->validateService->run($params);

        if (empty($errors)) {
            $item = new Item(
                new ItemId($params['id']),
                new ItemName($params['name']),
                new ItemAmount($params['amount'])
            );

            $isUpdated = $this->updateService->run($item);

            if (!$isUpdated) {
                $errors = [
                    'error' => $this->updateFailMessage
                ];
            }
        }

        return $this->responder->respond($errors, $item);
    }
}
