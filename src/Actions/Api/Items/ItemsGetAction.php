<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Actions\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Repositories\ItemRepository;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemsGetServiceFactory;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemsGetJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ItemsGetAction
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ItemRepository
     */
    protected $repository;

    /**
     * @var ItemsGetServiceFactory
     */
    protected $serviceFactory;

    /**
     * @var ItemsGetJsonResponder
     */
    protected $responder;

    public function __construct(
        Request $request,
        ItemRepository $repository,
        ItemsGetServiceFactory $serviceFactory,
        ItemsGetJsonResponder $responder
    ) {
        $this->request = $request;
        $this->repository = $repository;
        $this->serviceFactory = $serviceFactory;
        $this->responder = $responder;
    }

    public function __invoke(): JsonResponse
    {
        $service = $this->serviceFactory->make($this->request->get('type', null), $this->repository);

        $items = $service->run();

        return $this->responder->respond($items);
    }
}
