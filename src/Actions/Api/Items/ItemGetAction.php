<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Actions\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemGetService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateGetService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemGetJsonResponder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ItemGetAction
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ItemValidateGetService
     */
    protected $validateService;

    /**
     * @var ItemGetService
     */
    protected $getService;

    /**
     * @var ItemGetJsonResponder
     */
    protected $responder;

    public function __construct(
        Request $request,
        ItemValidateGetService $validateGetService,
        ItemGetService $getService,
        ItemGetJsonResponder $responder
    ) {
        $this->request = $request;
        $this->validateService = $validateGetService;
        $this->getService = $getService;
        $this->responder = $responder;
    }

    public function __invoke($id): JsonResponse
    {
        $item = null;

        $params = $this->request->all();
        $params['id'] = $id;

        $errors = $this->validateService->run($params);

        if (empty($errors)) {
            $itemId = new ItemId($params['id']);

            $item = $this->getService->run($itemId);
        }

        return $this->responder->respond($errors, $item);
    }
}
