<?php

namespace Gaqateq\RecruitmentBlueServicesServer\Actions\Api\Items;

use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemDeleteService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\Services\ItemValidateDeleteService;
use Gaqateq\RecruitmentBlueServicesServer\Domains\Items\ValueObjects\ItemId;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemDeleteJsonResponder;
use Gaqateq\RecruitmentBlueServicesServer\Responders\Items\ItemUpdateJsonResponder;
use Illuminate\Http\JsonResponse;

class ItemDeleteAction
{
    /**
     * @var ItemValidateDeleteService
     */
    protected $validateService;

    /**
     * @var ItemDeleteService
     */
    protected $deleteService;

    /**
     * @var ItemUpdateJsonResponder
     */
    protected $responder;

    /**
     * @var string|null
     */
    protected $deleteFailMessage = 'Unexpected error. Record not deleted.';

    public function __construct(
        ItemValidateDeleteService $validateService,
        ItemDeleteService $deleteService,
        ItemDeleteJsonResponder $responder,
        ?string $deleteFailMessage = null
    ) {
        $this->validateService = $validateService;
        $this->deleteService = $deleteService;
        $this->responder = $responder;
        if ($deleteFailMessage !== null) {
            $this->deleteFailMessage = $deleteFailMessage;
        }
    }

    public function __invoke($id): JsonResponse
    {
        $params = ['id' => $id];

        $errors = $this->validateService->run($params);

        if (empty($errors)) {
            $itemId = new ItemId($params['id']);

            $isDeleted = $this->deleteService->run($itemId);

            if (!$isDeleted) {
                $errors = [
                    'error' => $this->deleteFailMessage
                ];
            }
        }

        return $this->responder->respond($errors);
    }
}
